# coding=utf-8

from django.contrib import admin
from dictionary.models import Word

# Register your models here.

class WordAdmin(admin.ModelAdmin):
    list_display = ('verb', 'adverb', 'adjective', 'noun')
    search_fields = ('verb', 'adverb', 'adjective', 'noun')


admin.site.register(Word, WordAdmin)
