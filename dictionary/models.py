# coding=utf-8

from django.db import models
from django.contrib.auth.models import User


class Word(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    verb = models.CharField(max_length=255)
    adverb = models.CharField(max_length=255)
    adjective = models.CharField(max_length=255)
    noun = models.CharField(max_length=255)
    
    def __str__(self):
        return self.verb
