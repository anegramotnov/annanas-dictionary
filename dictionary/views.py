# coding=utf-8

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from dictionary.models import Word
from dictionary.form import WordForm
from dictionary.utils import lingualeo


@login_required
def index(request):
    saved = False
    deleted = False

    if request.method == 'POST':
        form = WordForm(request.POST)
        if form.is_valid():
            word = form.save(commit=False)
            word.user = request.user

            print("request user: %s" % request.user)

            lingualeo.add_word(request.user, word.verb, word.verb)

            word.save()
            saved = True
            form = WordForm()
    else:
        form = WordForm()

    word_list = Word.objects.filter(user=request.user).order_by('-id')

    if request.GET.get('deleted'):
        deleted = True

    context = {
        'word_list': word_list,
        'form': form,
        'saved': saved,
        'deleted': deleted
    }
    return render(request, 'index.html', context)


@login_required
def delete_word(request, word_id):
    deleted = False
    try:
        word = Word.objects.get(pk=word_id)
        word.delete()
        deleted = True
    except:
        pass
    url = reverse('index')
    if deleted:
        url = url + "?deleted=true"

    return HttpResponseRedirect(url)