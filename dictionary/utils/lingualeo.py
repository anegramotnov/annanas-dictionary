# coding=utf-8
__author__ = 'anegramotnov'

import urllib
import urllib2
import json
from cookielib import CookieJar

print u"Хуя"

class Lingualeo:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.cj = CookieJar()

    def auth(self):
        url = "http://api.lingualeo.com/api/login"
        values = {
            "email" : self.email,
            "password" : self.password
        }

        return self.get_content(url, values)

    def add_word(self, word, tword):
        url = "http://api.lingualeo.com/addword"
        values = {
            "word" : word.encode('utf-8'),
            "tword" : tword.encode('utf-8')
        }
        self.get_content(url, values)

    def get_translates(self, word):
        url = "http://api.lingualeo.com/gettranslates?word=" + word

        try:
            result = self.get_content(url, {})
            translate = result["translate"][0]
            return {
                "is_exist" : translate["is_user"],
                "word" : word,
                "tword" : translate["value"].encode("utf-8")
            }
        except Exception as e:
            return False

    def get_content(self, url, values):
        data = urllib.urlencode(values)

        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        req = opener.open(url, data)

        return json.loads(req.read())


def add_word(user, word, tword):
    ll = Lingualeo(user.email, user.first_name)
    res = ll.auth()
    print("Auth ll: %s" % (res,))
    res2 = ll.add_word(word, tword)
    print(res2)