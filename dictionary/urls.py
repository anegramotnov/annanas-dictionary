# coding=utf-8

__author__ = 'anegramotnov'

from django.conf.urls import patterns, url

from dictionary import views

urlpatterns = patterns('',
                       # url(r'^$', views.IndexView.as_view(), name='index'),
                       url(r'^$', views.index, name='index'),
                       # url(r'^add$', views.add, name='add'),
                       url(r'^delete_word/(?P<word_id>\d+)/$', views.delete_word, name='delete_word'),
)