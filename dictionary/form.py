# coding=utf-8

__author__ = 'anegramotnov'
from django import forms
from django.forms import ModelForm

from dictionary.models import Word


class WordForm(ModelForm):
    class Meta:
        model = Word

    def __init__(self, *args, **kwargs):
        super(WordForm, self).__init__(*args, **kwargs)
        self.fields['verb'].widget.attrs.update(
            {
                'class': 'form-control',
                # 'placeholder': 'Verb',
            }
        )
        self.fields['adverb'].widget.attrs.update(
            {
                'class': 'form-control',
                # 'placeholder': 'Adverb',
            }
        )
        self.fields['adjective'].widget.attrs.update(
            {
                'class': 'form-control',
                # 'placeholder': 'Adjective',
            }
        )
        self.fields['noun'].widget.attrs.update(
            {
                'class': 'form-control',
                # 'placeholder': 'Noun',
            }
        )

    # def save(self, *args, **kwargs):
    #     kwargs['commit'] = False
    #     obj = super(WordForm, self).save(*args, **kwargs)
    #     if self.request:
    #         obj.user = self.request.user
    #     obj.save()
    #     return obj